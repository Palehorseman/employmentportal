<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /admin/jobs_addedit.html.twig */
class __TwigTemplate_8fd1bd22a96bc06c8a9a4a86de848766ab3b6068136cffb864286d3cd87286cc extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "/admin/jobs_addedit.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "
    ";
        // line 5
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 6
            echo "        <ul>
            ";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 8
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 10
            echo "        </ul>
    ";
        }
        // line 12
        echo "    <body class=\"BG4\">
    <p class=\"banner\">Add Jobs</p>
    <br>
    <br>
    <form method=\"post\" enctype=\"multipart/form-data\">
        <input type=\"hidden\" name=\"id\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["jobInfo"]) ? $context["jobInfo"] : null), "id", []), "html", null, true);
        echo "\" />
        Name: <input type=\"text\" name=\"name\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["jobInfo"]) ? $context["jobInfo"] : null), "name", []), "html", null, true);
        echo "\"><br><br>
        Category: <select name=\"category\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["jobInfo"]) ? $context["jobInfo"] : null), "category", []), "html", null, true);
        echo "\">
            <option ";
        // line 20
        if (($this->getAttribute((isset($context["jobInfo"]) ? $context["jobInfo"] : null), "category", []) == "Agriculture, Food and Natural Resources")) {
            echo "selected";
        }
        echo ">Agriculture, Food and Natural Resources</option>
            <option ";
        // line 21
        if (($this->getAttribute((isset($context["jobInfo"]) ? $context["jobInfo"] : null), "category", []) == "Arts, Audio/Video Technology and Communications")) {
            echo "selected";
        }
        echo ">Arts, Audio/Video Technology and Communications</option>
            <option ";
        // line 22
        if (($this->getAttribute((isset($context["jobInfo"]) ? $context["jobInfo"] : null), "category", []) == "Education and Training")) {
            echo "selected";
        }
        echo ">Education and Training</option>
            <option ";
        // line 23
        if (($this->getAttribute((isset($context["jobInfo"]) ? $context["jobInfo"] : null), "category", []) == "Government and Public Administration")) {
            echo "selected";
        }
        echo ">Government and Public Administration</option>
            <option ";
        // line 24
        if (($this->getAttribute((isset($context["jobInfo"]) ? $context["jobInfo"] : null), "category", []) == "Hospitality and Tourism")) {
            echo "selected";
        }
        echo ">Hospitality and Tourism</option>
            <option ";
        // line 25
        if (($this->getAttribute((isset($context["jobInfo"]) ? $context["jobInfo"] : null), "category", []) == "Information Technology")) {
            echo "selected";
        }
        echo ">Information Technology</option>
            <option ";
        // line 26
        if (($this->getAttribute((isset($context["jobInfo"]) ? $context["jobInfo"] : null), "category", []) == "Manufacturing")) {
            echo "selected";
        }
        echo ">Manufacturing</option>
            <option ";
        // line 27
        if (($this->getAttribute((isset($context["jobInfo"]) ? $context["jobInfo"] : null), "category", []) == "Science, Technology, Engineering and Mathematics")) {
            echo "selected";
        }
        echo ">Science, Technology, Engineering and Mathematics</option>
        </select><br><br>
        Description: <input type=\"text\" name=\"description\" value=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["jobInfo"]) ? $context["jobInfo"] : null), "description", []), "html", null, true);
        echo "\">
        <br>
        <br>
        <input type=\"submit\" value=\"";
        // line 32
        if (($this->getAttribute((isset($context["jobInfo"]) ? $context["jobInfo"] : null), "id", []) !=  -1)) {
            echo "Save";
        } else {
            echo "Add";
        }
        echo " job\">
    </form>
    <div class=\"main1\">
        <img src=\"uploads/jobboard.jpg\" alt=\"Job board\">
    </div>
    </body>
";
    }

    public function getTemplateName()
    {
        return "/admin/jobs_addedit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 32,  135 => 29,  128 => 27,  122 => 26,  116 => 25,  110 => 24,  104 => 23,  98 => 22,  92 => 21,  86 => 20,  82 => 19,  78 => 18,  74 => 17,  67 => 12,  63 => 10,  54 => 8,  50 => 7,  47 => 6,  45 => 5,  42 => 4,  39 => 3,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block content %}

    {% if errorList %}
        <ul>
            {% for error in errorList %}
                <li>{{error}}</li>
            {% endfor %}
        </ul>
    {% endif %}
    <body class=\"BG4\">
    <p class=\"banner\">Add Jobs</p>
    <br>
    <br>
    <form method=\"post\" enctype=\"multipart/form-data\">
        <input type=\"hidden\" name=\"id\" value=\"{{jobInfo.id}}\" />
        Name: <input type=\"text\" name=\"name\" value=\"{{ jobInfo.name }}\"><br><br>
        Category: <select name=\"category\" value=\"{{ jobInfo.category }}\">
            <option {% if jobInfo.category == \"Agriculture, Food and Natural Resources\" %}selected{% endif %}>Agriculture, Food and Natural Resources</option>
            <option {% if jobInfo.category == \"Arts, Audio/Video Technology and Communications\" %}selected{% endif %}>Arts, Audio/Video Technology and Communications</option>
            <option {% if jobInfo.category == \"Education and Training\" %}selected{% endif %}>Education and Training</option>
            <option {% if jobInfo.category == \"Government and Public Administration\" %}selected{% endif %}>Government and Public Administration</option>
            <option {% if jobInfo.category == \"Hospitality and Tourism\" %}selected{% endif %}>Hospitality and Tourism</option>
            <option {% if jobInfo.category == \"Information Technology\" %}selected{% endif %}>Information Technology</option>
            <option {% if jobInfo.category == \"Manufacturing\" %}selected{% endif %}>Manufacturing</option>
            <option {% if jobInfo.category == \"Science, Technology, Engineering and Mathematics\" %}selected{% endif %}>Science, Technology, Engineering and Mathematics</option>
        </select><br><br>
        Description: <input type=\"text\" name=\"description\" value=\"{{ jobInfo.description }}\">
        <br>
        <br>
        <input type=\"submit\" value=\"{% if jobInfo.id != -1 %}Save{% else %}Add{% endif %} job\">
    </form>
    <div class=\"main1\">
        <img src=\"uploads/jobboard.jpg\" alt=\"Job board\">
    </div>
    </body>
{% endblock content %}
", "/admin/jobs_addedit.html.twig", "C:\\xampp\\htdocs\\employmentportal\\templates\\admin\\jobs_addedit.html.twig");
    }
}
