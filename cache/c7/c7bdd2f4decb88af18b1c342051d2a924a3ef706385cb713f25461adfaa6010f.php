<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /jobsearch/job_search.html.twig */
class __TwigTemplate_c90b20df44711dc674af784bec27fd1588fd4bd4a7ba73d8d63608a586eee3e8 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'headAdd' => [$this, 'block_headAdd'],
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "/jobsearch/job_search.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_headAdd($context, array $blocks = [])
    {
        // line 5
        echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
";
    }

    // line 7
    public function block_title($context, array $blocks = [])
    {
        echo "Job Search";
    }

    // line 9
    public function block_content($context, array $blocks = [])
    {
        // line 10
        echo "    <body class=\"main3\">
    <p class=\"banner\">What is the name of the job you are looking for?</p>
    <br>
    <br>
    <form method=\"get\" action=\"/search\">
        <input type=\"text\" name=\"query\" />
        <input type=\"submit\" value=\"Search\" />
    </form>
    <img src=\"uploads/jobsearch61.jpg\" alt=\"Job hunt\">
    </body>
";
    }

    public function getTemplateName()
    {
        return "/jobsearch/job_search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 10,  55 => 9,  49 => 7,  44 => 5,  41 => 4,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}


{% block headAdd %}
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
{% endblock headAdd %}
{% block title %}Job Search{% endblock %}

{% block content %}
    <body class=\"main3\">
    <p class=\"banner\">What is the name of the job you are looking for?</p>
    <br>
    <br>
    <form method=\"get\" action=\"/search\">
        <input type=\"text\" name=\"query\" />
        <input type=\"submit\" value=\"Search\" />
    </form>
    <img src=\"uploads/jobsearch61.jpg\" alt=\"Job hunt\">
    </body>
{% endblock content %}
", "/jobsearch/job_search.html.twig", "C:\\xampp\\htdocs\\employmentportal\\templates\\jobsearch\\job_search.html.twig");
    }
}
