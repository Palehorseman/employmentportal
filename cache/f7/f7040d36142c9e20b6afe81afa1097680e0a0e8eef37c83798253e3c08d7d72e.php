<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_39d5cfe2db36f8d6bc7e2aca5feddf67e2d0798d90cf73891e92387fa518706a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'headAdd' => [$this, 'block_headAdd'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        echo "Job Portal";
    }

    // line 3
    public function block_headAdd($context, array $blocks = [])
    {
        // line 4
        echo "    <script>
 
    </script>
        <title>Job Portal</title>
        <link rel=\"stylesheet\" href=\"styles.css\">
 ";
    }

    // line 11
    public function block_content($context, array $blocks = [])
    {
        // line 12
        echo "    <body class=\"main\">
    <div class =\"main1\">
        <img src=\"uploads/jobboard1.jpg\" alt=\"Job board\">
    </div>
    <p class=\"banner\">The fast and easy way to search for jobs</p>
    <br>
    <br>
    <div id=\"pageNavigation\">
    <p><a href=\"/login\">Login</a></p>
    <p><a href=\"/register\">Register</a></p>
    ";
        // line 22
        if ((isset($context["isRecruiter"]) ? $context["isRecruiter"] : null)) {
            // line 23
            echo "        <p><a href=\"/jobs_addedit\">Add Job Posting</a></p>
    ";
        }
        // line 25
        echo "    <p><a href=\"/logout\">Logout</a></p>
    </div>
    </body>
";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 25,  76 => 23,  74 => 22,  62 => 12,  59 => 11,  50 => 4,  47 => 3,  41 => 2,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block title %}Job Portal{% endblock %}
{% block headAdd %}
    <script>
 
    </script>
        <title>Job Portal</title>
        <link rel=\"stylesheet\" href=\"styles.css\">
 {% endblock %}    
 
{% block content %}
    <body class=\"main\">
    <div class =\"main1\">
        <img src=\"uploads/jobboard1.jpg\" alt=\"Job board\">
    </div>
    <p class=\"banner\">The fast and easy way to search for jobs</p>
    <br>
    <br>
    <div id=\"pageNavigation\">
    <p><a href=\"/login\">Login</a></p>
    <p><a href=\"/register\">Register</a></p>
    {% if isRecruiter %}
        <p><a href=\"/jobs_addedit\">Add Job Posting</a></p>
    {% endif %}
    <p><a href=\"/logout\">Logout</a></p>
    </div>
    </body>
{% endblock content %}
  ", "index.html.twig", "C:\\xampp\\htdocs\\employmentportal\\templates\\index.html.twig");
    }
}
