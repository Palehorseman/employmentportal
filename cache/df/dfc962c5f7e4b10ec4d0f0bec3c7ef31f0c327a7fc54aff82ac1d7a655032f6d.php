<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /admin/jobs_delete_success.html.twig */
class __TwigTemplate_4f451b018f9720dbed300401cbbd220fb11696befc39920e97806d0d2e32174e extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "/admin/jobs_delete_success.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "<div id=\"BG4\">
    <p>Job deleted successfully,
        <br>
        <a href=\"/admin/jobs_list\">click to continue</a></p>
</div>
";
    }

    public function getTemplateName()
    {
        return "/admin/jobs_delete_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 4,  39 => 3,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block content %}
<div id=\"BG4\">
    <p>Job deleted successfully,
        <br>
        <a href=\"/admin/jobs_list\">click to continue</a></p>
</div>
{% endblock %}
", "/admin/jobs_delete_success.html.twig", "C:\\xampp\\htdocs\\employmentportal\\templates\\admin\\jobs_delete_success.html.twig");
    }
}
