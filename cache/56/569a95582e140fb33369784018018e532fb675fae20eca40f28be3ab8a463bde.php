<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/jobs_list.html.twig */
class __TwigTemplate_49cce6f02b04d7bd6529f47b73632e54eb33329f101f0f5f829b852329830a20 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "admin/jobs_list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        echo "Jobs list";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        // line 6
        echo "    <body class=\"BG4\">
    <table border=\"1\">
        <tr><th>#</th><th>name</th><th>category</th><th>description</th></tr>
        ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["allJobs"]) ? $context["allJobs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
            // line 10
            echo "        <tr>
            <td>";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "id", []), "html", null, true);
            echo "</td>
            <td>";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "name", []), "html", null, true);
            echo "</td>
            <td>";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "category", []), "html", null, true);
            echo "</td>
            <td>";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "description", []), "html", null, true);
            echo "</td>
            <td>
                <!-- method 1 - simple h-ref text link -->
                <!-- <a href=\"/admin/products/edit/";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "id", []), "html", null, true);
            echo "\">Edit</a> -->
                <!-- method 2 - button with javascript -->
                <button onclick=\"window.location='/jobs_delete?id=";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "id", []), "html", null, true);
            echo "';\">Delete</button>
                <!-- method 3 - form with submit button -->
                <button onclick=\"window.location='/jobs_addedit?id=";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["j"], "id", []), "html", null, true);
            echo "';\">Edit</button>
                </form>
            </td>
        </tr>                
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "    </table>
    <br>
    <br>
    <p><a href=\"/\">Return to homepage</a></p>
    <p><a href=\"/register\">Go to registration</a></p>
    </body>
";
    }

    public function getTemplateName()
    {
        return "admin/jobs_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 26,  89 => 21,  84 => 19,  79 => 17,  73 => 14,  69 => 13,  65 => 12,  61 => 11,  58 => 10,  54 => 9,  49 => 6,  46 => 5,  40 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Jobs list{% endblock %}

{% block content %}
    <body class=\"BG4\">
    <table border=\"1\">
        <tr><th>#</th><th>name</th><th>category</th><th>description</th></tr>
        {% for j in allJobs %}
        <tr>
            <td>{{j.id}}</td>
            <td>{{j.name}}</td>
            <td>{{j.category}}</td>
            <td>{{j.description}}</td>
            <td>
                <!-- method 1 - simple h-ref text link -->
                <!-- <a href=\"/admin/products/edit/{{j.id}}\">Edit</a> -->
                <!-- method 2 - button with javascript -->
                <button onclick=\"window.location='/jobs_delete?id={{j.id}}';\">Delete</button>
                <!-- method 3 - form with submit button -->
                <button onclick=\"window.location='/jobs_addedit?id={{j.id}}';\">Edit</button>
                </form>
            </td>
        </tr>                
        {% endfor %}
    </table>
    <br>
    <br>
    <p><a href=\"/\">Return to homepage</a></p>
    <p><a href=\"/register\">Go to registration</a></p>
    </body>
{% endblock content %}
", "admin/jobs_list.html.twig", "C:\\xampp\\htdocs\\employmentportal\\templates\\admin\\jobs_list.html.twig");
    }
}
