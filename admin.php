<?php

if (false) {
    $app = new \Slim\Slim();
}

$app->get('/admin/jobs_list', function() use ($app) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    $list = DB::query("SELECT * FROM jobs");
    $app->render('admin/jobs_list.html.twig', array('list' => $list));
});

// STATE 1: first show
$app->get('/admin/jobs/:action(/:id)', function($action, $id = 0) use ($app) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    if (($action == 'add' && $id != 0) || ($action == 'edit' && $id == 0)) {
        $app->notFound(); // 404 page
        return;
    }
    if ($action == 'add') {
        $app->render('admin/jobs_addedit.html.twig');
    } else { // edit
        $job = DB::queryFirstRow("SELECT * FROM jobs WHERE id=%i", $id);
        if (!$job) {
            $app->notFound();
            return;
        }
        $app->render('admin/jobs_addedit.html.twig', array('v' => $job));
    }
})->conditions(array('action' => '(add|edit)'));

$app->post('/admin/jobs/:action(/:id)', function($action, $id = 0) use ($app) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    if (($action == 'add' && $id != 0) || ($action == 'edit' && $id == 0)) {
        $app->notFound(); // 404 page
        return;
    }
    //
    $name = $app->request()->post('name');
    $category = $app->request()->post('category');
    //
    $errorList = array();
    // FIXME: sanitize html tags in name and category
    if (strlen($name) < 2 || strlen($name) > 100) {
        array_push($errorList, "Name must be 2-100 characters long");
        $name = "";
    }
    if (strlen($category) < 2 || strlen($description) > 100) {
        array_push($errorList, "Category must be 2-100 characters long");
        $category = "";
    }
    if ($errorList) { // STATE 2: failed submission
        $app->render('admin/jobs_addedit.html.twig', array(
            'errorList' => $errorList,
            'v' => array('id' => $id,
                'name' => $name, 'category' => $category)));
    } else { // STATE 3: successful submission
        if ($action == 'add') {
            DB::insert('jobs', array('name' => $name, 'category' => $category));
            $app->render('admin/jobs_addedit_success.html.twig');
        } else {
            DB::update('jobs', array('name' => $name, 'description' => $description), 'id=%i', $id);
            $app->render('admin/jobs_addedit_success.html.twig', array('savedId' => $id));
        }
    }
});

$app->get('/admin/company/list', function() use ($app) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    $list = DB::query("SELECT * FROM companies");
    $app->render('admin/company_list.html.twig', array('list' => $list));
});

// STATE 1: first show
$app->get('/admin//:action(/:id)', function($action, $id = 0) use ($app) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    if (($action == 'add' && $id != 0) || ($action == 'edit' && $id == 0)) {
        $app->notFound(); // 404 page
        return;
    }
    if ($action == 'add') {
        $app->render('admin/company_addedit.html.twig');
    } else { // edit
        $companies = DB::queryFirstRow("SELECT * FROM companies WHERE id=%i", $id);
        if (!$companies) {
            $app->notFound();
            return;
        }
        $app->render('admin/company_addedit.html.twig', array('v' => $companies));
    }
})->conditions(array('action' => '(add|edit)'));

$app->post('/admin/company/:action(/:id)', function($action, $id = 0) use ($app) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    if (($action == 'add' && $id != 0) || ($action == 'edit' && $id == 0)) {
        $app->notFound(); // 404 page
        return;
    }
    //
    $companyname = $app->request()->post('companyname');
    $profiledescription = $app->request()->post('profiledescription');
    $categoryid = $app->request()->post('categoryid');
    $website = $app->request()->post('website');
    
    $errorList = array();
    // FIXME: sanitize html tags in companyname, profiledescription, categoryid and website
    if (strlen($companyname) < 2 || strlen($companyname) > 100) {
        array_push($errorList, "Company name must be 2-100 characters long");
        $companyname = "";
    }
    if (strlen($profiledescription) < 10 || strlen($profiledescription) > 1000) {
        array_push($errorList, "Profile description must be 10-1000 characters long");
        $profiledescription = "";
    }
    if ($errorList) { // STATE 2: failed submission
        $app->render('admin/company_addedit.html.twig', array(
            'errorList' => $errorList,
            'v' => array('id' => $id,
                'companyname' => $companyname, 'profiledescription' => $profiledescription,
                'categoryid' => $categoryid, 'website' => $website)));
    } else { // STATE 3: successful submission
        if ($action == 'add') {
            DB::insert('companies', array('companyname' => $companyname, 'profiledescription' => $profiledescription,
                'categoryid' => $categoryid, 'website' => $website));
            $app->render('admin/company_addedit_success.html.twig');
        } else {
            DB::update('companies', array('companyname' => $companyname, 'profiledescription' => $profiledescription,
                'categoryid' => $categoryid, 'website' => $website), 'id=%i', $id);
            $app->render('admin/company_addedit_success.html.twig', array('savedId' => $id));
        }
    }
});