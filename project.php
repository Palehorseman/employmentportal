<?php

//require_once 'db.php';

session_cache_limiter(false);
session_start();

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

DB::$user = 'jobportal';
DB::$password = '123456';
DB::$dbName = 'jobportal';
DB::$host = "127.0.0.1";
DB::$encoding = 'utf8';
DB::$port = 3333;
DB::$error_handler = 'database_error_handler';
DB::$nonsql_error_handler = 'database_error_handler';

function database_error_handler($params) {
    global $app, $log;
    $log->error("SQL Error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL Query: " . $params['query']);
    }
    $app->render("internal_error.html.twig");
    http_response_code(500);
    die(); // don't want to keep going if a query broke
}

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

\Slim\Route::setDefaultConditions(array(
    'id' => '[1-9][0-9]*'
));

function getUserIpAddr() {
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

$app->get('/', function() use ($app, $log) {
    
    $app->render('index.html.twig'); 
           
});

$app->get('/jobs_delete', function() use ($app) {
    $id = $app->request()->get('id');
    
    DB::delete('jobs', "id=%s", $id);
    $app->render('/admin/jobs_delete_success.html.twig'); 
});

$app->get('/jobs_edit', function() use ($app) {
    $id = $app->request()->get('id');
    $app->render('/admin/jobs_addedit');
});

$app->get('/search', function() use ($app) {
    $query = $app->request()->post('query'); 
    $allJobs = DB::query("SELECT * FROM jobs");
    $outputJobs = array();
    
    if ($query == "") {
        $outputJobs = $allJobs;
    }
    else {
        foreach ($allJobs as $job) {
            if (stripos($job['name'], $query) !== false) {  
                array_push($outputJobs, [
                    'id' => $job['id'],
                    'name' => $job['name'],
                    'description' => $job['description'],
                    'category' => $job['category'],
                    'datePosted' => $job['Dateposted']
                ]);
            }
        }
    }
    
    $app->render("admin/jobs_list.html.twig", [
        'allJobs' => $outputJobs
    ]);
})->name("search_route");

$app->get('/login', function() use ($app, $log) {  
    $isRecruiter = false;
    if (isset($_SESSION['user']) && $_SESSION['user']['userType'] == "Recruiter") {
        $isRecruiter = true;
    }
    
    $app->render('login.html.twig', [
        'isRecruiter' => $isRecruiter
    ]);  
});

$app->post('/login', function() use ($app, $log) {
    $email = $app->request()->post('email');
    $password = $app->request()->post('password');
    //
    $loginSuccessful = false;
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);    
    if ($user) {
        if ($user['password'] == $password) {
            $loginSuccessful = true;
        }        
    }    
    //
    if (!$loginSuccessful) { // array not empty -> errors present
        $log->info(sprintf("Login failed, email=%s, from IP=%s", $email, getUserIpAddr()));
        $app->render('login.html.twig', array('error' => true));
    } else { // STATE 3: successful submission
        unset($user['password']);
        $_SESSION['user'] = $user;
        $redirectUrl = $user['userType'] == "Recruiter" ? "/jobs_addedit" : "/job_search";
        
        $log->info(sprintf("Login successful, email=%s, from IP=%s", $email, getUserIpAddr()));
        $app->render('login_success.html.twig', [
            'redirectUrl' => $redirectUrl
        ]);
    }
});

$app->get('/logout', function() use ($app) {
    unset($_SESSION['user']);
    $app->render('logout.html.twig');
});

$app->get('/session', function() {
    echo '<pre>';
    print_r($_SESSION);
});

$app->get('/job_search', function() use ($app) {
    $app->render('/jobsearch/job_search.html.twig');
});

$app->get('/jobs_addedit', function() use ($app) {
    $id = $app->request()->get('id');
    $jobInfo = array();
    
    if ($id == null) {
        $jobInfo["id"] = -1;
    }
    else {
        $jobInfo = DB::queryFirstRow("SELECT id, name, description, category FROM jobs WHERE id=%s", $id);
    }

    $app->render('/admin/jobs_addedit.html.twig', array(
        'jobInfo' => $jobInfo
    ));
});

$app->get('/jobs_addedit_success', function() use ($app) {
    $app->render('/admin/jobs_addedit_success.html.twig');
});

$app->get('/admin/jobs_list', function() use ($app) {
    $app->response->redirect($app->urlFor('search_route'), 303);
});

$app->get('/jobs_delete_success', function() use ($app) {
    $app->render('/admin/jobs_delete_success.html.twig');
});
   

$app->post('/jobs_addedit', function() use ($app) {
    $Name = $app->request()->post('name');
    $Description = $app->request()->post('description');
    $Category = $app->request()->post('category');
    $id = $app->request()->post('id');
    
    if ($id == -1) {
        $id = DB::queryFirstField("SELECT id FROM jobs ORDER BY id DESC");
        $id++;
    }
    
    $error = false;
    
    if(strlen($Name) > 100 || strlen($Name) < 1){
    echo "Name has to be between 1-100 characters";
    $error = true;
} 
if(strlen($Description) < 1){
    echo "Description has to be at least 1 character long.";
    $error = true;
    }

    if(!$error) {
    DB::insertUpdate('jobs', array('id' => $id, 'name' => $Name, 'description' => $Description, 'category' => $Category));   
    
    $app->response->redirect($app->urlFor('search_route'), 303);
        
}
else {
    echo "Unable to create an account";
    die();
}
});
// STATE 1: first show

$app->get('/register', function() use ($app, $log) {
    $app->render('register.html.twig', array('sessionUser' => @$_SESSION['user']));
           
 });

$app->post('/register', function() use ($app, $log) {
    $error = false;
    
    $userType = $app->request()->post('usertype') == "Job Seeker" ? 1 : 2;
    $firstName = $app->request()->post('firstName');
    $lastName = $app->request()->post('lastName');
    $email = $app->request()->post('email');
    $password = $app->request()->post('password');
    $birthDate = $app->request()->post('birthdate');
    $gender = $app->request()->post('gender');
    $phone = $app->request()->post('phone');
    
    $photo = "";

    if (isset($_FILES['photo'])) {
        // File was uploaded
        $extension = substr($_FILES['photo']['name'], strripos($_FILES['photo']['name'], '.'));
        $fileName = md5($_FILES['photo']['tmp_name'] . mt_rand(0, 100)) . $extension;
        
        if (move_uploaded_file($_FILES['photo']['tmp_name'], 'uploads/user/' . $fileName)) {            
            $photo = $fileName;
        }
        else {
            $error = true;
            echo "Error uploading profile image!";
        }
    }
    
    
    
if($userType != 1 && $userType != 2){
echo "You must choose either Job Seeker or Recruiter";
$error = true;
}    

if(strlen($firstName) > 60){
    echo "First name cannot exceed 60 characters";
    $error = true;
}

if(strlen($lastName) > 60){
    echo "First name cannot exceed 60 characters";
    $error = true;
}



if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        echo "Please enter a valid email";
        $error = true;
}

$emailCheck = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
if ($emailCheck != null) {
    echo("User with that email already exists!");
    $error = true;
}

 if ((strlen($password) < 6)
                || (preg_match("/[A-Z]/", $password) == FALSE )
                || (preg_match("/[a-z]/", $password) == FALSE )
                || (preg_match("/[0-9]/", $password) == FALSE )) 
         {
     echo "Password must be at least 6 characters long and have at least one uppercase and lowercase letter and at"
     . " least one number.";
     $error = true;
 }
 
 if ((strlen($birthDate) > 12)){
     echo "Birthdate field cannot exceed 12 characters.";
     $error = true;
 }
 
 if($gender != "male" && $gender != "female" && $gender != "other"){
    echo "You only have 3 choices for gender";
    $error = true;  
    }

if(strlen($phone) < 10 || strlen($phone) > 20) {
    echo "Please enter a valid phone number";
    $error = true;
}  

if(!$error) {
    DB::insert('users', array('usertype' => $userType, 'firstname' => $firstName, 'lastname' => $lastName, 'email' => $email, 'password' => $password, 'birthdate' => $birthDate, 
        'gender' => $gender, 'phone' => $phone, 'photo' => $photo));   
    
    $app->render('register_success.html.twig');
   

}
else {
    echo " Unable to create an account";
    die();
}

});

$app->post('/', function() use ($app) {
$request = $app->request;
$searchterm = $request->post('searchterm');

$app->response->redirect($app->urlFor('search', [
    'searchterm' => $searchterm
]));
})->name('search');

?>

<?php

$app->run();

